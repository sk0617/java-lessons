# README #

Series of java exercises

### Exercise List ###

* Hello World
* Decimal to binary converter
* Quadratic equation solver
* Greatest Common Divisor of 5 numbers
* Convert Roman numerals to numbers
* Indicate if a number is prime
* Indicate if a sentence/word is a palindrome
* Order an array using bubble sort
* Order an array using quicksort
* Matrix multiplication
